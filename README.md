[![Project Status: Unsupported - The project has reached a stable, usable state but the author(s) have ceased all work on it. A new maintainer may be desired.](http://www.repostatus.org/badges/0.1.0/unsupported.svg)](http://www.repostatus.org/#unsupported)

# brainfuck

A [brainfuck](https://en.wikipedia.org/wiki/Brainfuck) interpreter for R.

### Installation

To install, you first need the 
[*devtools*](http://cran.r-project.org/web/packages/devtools/index.html) package.

```{r}
install.packages("devtools")
```

Then you can install the *brainfuck* package using

```{r}
install_bitbucket(
  "richierocks/brainfuck"
)
```